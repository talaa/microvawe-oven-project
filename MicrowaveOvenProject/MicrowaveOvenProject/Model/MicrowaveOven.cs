﻿using System;
using System.ComponentModel;

namespace MicrowaveOvenProject.Model
{
    public partial class MicrowaveOven : IMicrowaveOvenHW, INotifyPropertyChanged
    {
        #region LightState enum

        public enum LightState
        {
            On,
            Off
        }

        #endregion

        #region Constants
        private const double DefaultInterval = 1_000;
        public const int MinuteInMilliseconds = 60_000;
        #endregion

        #region Private fileds
        private MicrowaveTimer timer;

        private bool _doorOpen;
        private LightState _light;
        #endregion

        #region Properties
        public bool DoorOpen
        {
            get =>_doorOpen;

            private set
            {
                _doorOpen = value;
                OnPropertyChanged(nameof(DoorOpen));
            }
        }

        public LightState Light
        {
            get => _light;
            private set => _light = value;
        }

        public bool HeaterRunning
        {
            get => timer.Enabled;
        }

        public int CurrentTimeValue
        {
            get => (int)timer.CurrentValue;
        }
        #endregion

        #region Events declaration
        public event Action<bool> DoorOpenChanged;
        public event EventHandler StartButtonPressed;
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Constructors
        public MicrowaveOven(double maxValueOfTimer, double interval = DefaultInterval)
        {
            timer = new MicrowaveTimer(maxValueOfTimer, interval);
            _light = LightState.Off;
            DoorOpenChanged += OpenOrCloseDoorAction;
            PropertyChanged += DoorOpenPropertyChangedEventHandler;
            StartButtonPressed += StartButtonPressedEventHandler;
        }
        #endregion

        #region Private methods

        private void IncreaseTime(double value)
        {
            timer.AddTime(value);
        }

        #endregion

        #region Public methods

        public void TurnOnHeater()
        {
            timer.Start(MinuteInMilliseconds);
        }

        public void TurnOffHeater()
        {
            timer.Stop();
        }

        public void OpenOrCloseDoorAction(bool open)
        {
            if (!DoorOpen.Equals(open))
            {
                DoorOpen = open;
            }
        }

        public void PressStartButton()
        {
            if (!DoorOpen)
                OnStartButtonPressed(new EventArgs());
        }

        #endregion

        #region Methods to rise events

        private void OnStartButtonPressed(EventArgs e)
        {
            EventHandler handler = StartButtonPressed;

            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void OnPropertyChanged(String propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region Events handlers

        private void StartButtonPressedEventHandler(object sender, EventArgs e)
        {
            if (timer.Enabled)
            {
                IncreaseTime(MinuteInMilliseconds);
            }
            else
            {
                TurnOnHeater();
            }
        }


        private void DoorOpenPropertyChangedEventHandler(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(nameof(DoorOpen)))
            {
                _light = DoorOpen ? LightState.On : LightState.Off;

                if (timer.Enabled)
                {
                    timer.Stop();
                }
            }
        }
        #endregion
    }
}
