﻿using System;
using System.Timers;

namespace MicrowaveOvenProject.Model
{
    public interface IMicrowaveTimer
    {
        int MaxTimeValue { get; set; }
        int CurrentValue { get; set; }
        bool Enabled { get; }
        double Interval { get; set; }
        event ElapsedEventHandler Elapsed;
        event EventHandler TimerStopped;
        void AddTime(double value);
    }
}
