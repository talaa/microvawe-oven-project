﻿using System;
using System.Timers;
using MicrowaveOvenProject.CustomExceptions;

namespace MicrowaveOvenProject.Model
{
    public partial class MicrowaveOven
    {
        private class MicrowaveTimer : Timer
        {
            #region Private fields
            static double _maxValue;
            static double _currentValue;
            #endregion

            #region Properties

            public double CurrentValue
            {
                get => _currentValue;

                private set
                {
                    if (value > _maxValue)
                    {
                        throw new InvalidValueException("Current timer value can not be greater than max timer value.");
                    }

                    _currentValue = value;
                }
            }

            #endregion

            #region Events declaration
            private event EventHandler TimerStopped;
            #endregion

            #region Constructors

            /// <summary>
            /// MicrowaveTimer Constructor
            /// </summary>
            /// <param name="maxValueOfTimer">Max value, that timer can have, expressed in milliseconds.</param>
            /// <param name="interval">Timer interval value, expressed in milliseconds.</param>
            public MicrowaveTimer(double maxValueOfTimer, double interval = DefaultInterval) : base()
            {
                base.Interval = interval;
                _maxValue = maxValueOfTimer;
                TimerStopped += TimerStoppedEvent;
            }

            #endregion

            #region Public methods

            /// <summary>
            /// Start MicrowaveTimer by specified time.
            /// </summary>
            /// <param name="value">Time value, expressed in milliseconds</param>
            public void Start(double value)
            {
                CurrentValue = value;
                base.Elapsed += TickEvent;
                base.Enabled = true;
                base.Start();
            }

            /// <summary>
            /// Stop MicrowaveTimer
            /// </summary>
            public new void Stop()
            {
                OnStopTimer(new EventArgs());
            }

            /// <summary>
            /// Add time to MicrowaveTimer currentValue
            /// </summary>
            /// <param name="value">Time value, expressed in milliseconds.</param>
            public void AddTime(double value)
            {
                CurrentValue += value;
            }

            #endregion

            #region Methods to rise events

            private void OnStopTimer(EventArgs e)
            {
                EventHandler handler = TimerStopped;

                if (handler != null)
                    handler(this, e);
            }

            #endregion

            #region Events handlers

            /// <summary>
            /// Signal if the interval time elapses.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void TickEvent(object sender, ElapsedEventArgs e)
            {
                if(CurrentValue > 0)
                {
                    CurrentValue -= base.Interval;
                }
                else
                {
                    this.Stop();
                }
            }

            /// <summary>
            /// Signal if the time already elapsed.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void TimerStoppedEvent(object sender, EventArgs e)
            {
                base.Elapsed -= TickEvent;
                base.Enabled = false;
                base.Stop();
                CurrentValue = 0;
            }

            #endregion
        }
    }
}
