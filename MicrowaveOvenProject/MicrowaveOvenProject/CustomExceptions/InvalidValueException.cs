﻿using System;

namespace MicrowaveOvenProject.CustomExceptions
{
    public class InvalidValueException : Exception
    {
        public InvalidValueException(String message) : base(message) { }
    }
}
