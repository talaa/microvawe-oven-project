﻿using System;

namespace MicrowaveOvenProject.CustomExceptions
{
    public class NotFoundKeyException : Exception
    {
        public String AdditionalInfo { get; private set; }

        public NotFoundKeyException(String message, String additionalInfo = null) : base(message)
        {
            AdditionalInfo = additionalInfo;
        }
    }
}
