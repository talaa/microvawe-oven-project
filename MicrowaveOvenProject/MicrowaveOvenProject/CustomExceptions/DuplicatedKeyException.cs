﻿using System;

namespace MicrowaveOvenProject.CustomExceptions
{
    public class DuplicatedKeyException : Exception
    {
        public DuplicatedKeyException(String message) : base(message) { }
    }
}
