﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MicrowaveOvenProject.Controller;
using MicrowaveOvenProject.Model;

namespace MicrowaveOvenProject
{
    class Program
    {
        static void Main(string[] args)
        {
            //Example of using MicrowavesOwenController

            MicrowavesOvenController controller = new MicrowavesOvenController();

            String key = controller.CreateNewMicrowaveOven(MicrowaveOven.MinuteInMilliseconds * 10);

            controller.OpenDoor(key);
            bool doorState = controller.GetDoorState(key);
            MicrowaveOven.LightState lightState = controller.GetLightState(key);

            Console.WriteLine("MicrowaveOven named {0} has {1} door and its light is turn {2}."
                ,key, doorState ? "opened" : "closed", lightState);

            controller.CloseDoor(key);
            doorState = controller.GetDoorState(key);
            lightState = controller.GetLightState(key);

            Console.WriteLine("MicrowaveOven named {0} has {1} door and its light is turn {2}."
                , key, doorState ? "opened" : "closed", lightState);

            controller.PressStartButton(key);
            controller.PressStartButton(key);
            bool isMicrowenOvenRunning = controller.IsMicrowaveOvenRunning(key);
            int timerCurrentValue = controller.GetTimerCurrentValue(key);
            Console.WriteLine("MicrowaveOven named {0} is {1} and the timer current value is {2} minutes."
                , key, isMicrowenOvenRunning ? "running" : "not running", timerCurrentValue / MicrowaveOven.MinuteInMilliseconds);

            controller.OpenDoor(key);
            isMicrowenOvenRunning = controller.IsMicrowaveOvenRunning(key);
            timerCurrentValue = controller.GetTimerCurrentValue(key);
            Console.WriteLine("MicrowaveOven named {0} is {1} and the timer current value is {2} minutes."
                , key, isMicrowenOvenRunning ? "running" : "not running", timerCurrentValue / MicrowaveOven.MinuteInMilliseconds);

            controller.RemoveMicrowaveOven(key);

            Console.ReadKey();
        }
    }
}
