﻿using System;
using System.Collections.Generic;
using MicrowaveOvenProject.Model;
using MicrowaveOvenProject.CustomExceptions;

namespace MicrowaveOvenProject.Controller
{
    public class MicrowavesOvenController
    {
        private Dictionary<String, MicrowaveOven> microwaveOvensDictionary;
        private const String Prefix = "microOven_";

        public MicrowavesOvenController()
        {
            microwaveOvensDictionary = new Dictionary<String, MicrowaveOven>();
        }

        #region Private methods

        private String AddMicrowaveOven(MicrowaveOven microwaveOven, String key = null)
        {
            if (String.IsNullOrEmpty(key))
            {
                key = GetNextAvailableKey(microwaveOvensDictionary.Count + 1);
            }
            else if (microwaveOvensDictionary.ContainsKey(key))
            {
                throw new DuplicatedKeyException("The key value has already exists.");
            }

            microwaveOvensDictionary.Add(key, microwaveOven);

            return key;
        }

        private String GetNextAvailableKey(int number)
        {
            if (microwaveOvensDictionary.ContainsKey(Prefix + number))
            {
                return GetNextAvailableKey(++number);
            }
            else
            {
                return Prefix + number;
            }
        }

        private MicrowaveOven GetMicrowaveOven(String key)
        {
            MicrowaveOven microwaveOven;

            if (!microwaveOvensDictionary.TryGetValue(key, out microwaveOven))
            {
                throw new NotFoundKeyException("The key value does not exists.");
            }

            return microwaveOven;
        }

        #endregion

        #region Properties

        public int Count
        {
            get => microwaveOvensDictionary.Count;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Creates new MicrowaveOven.
        /// </summary>
        /// <param name="maxValueOfTimer">Max value, that timer can have, expressed in milliseconds</param>
        /// <param name="key">Unique name of MicrowaveOven</param>
        /// <returns>Returns unique name of MicrowaveOven</returns>
        public String CreateNewMicrowaveOven(int maxValueOfTimer, String key = null)
        {
            MicrowaveOven microwaveOven = new MicrowaveOven(maxValueOfTimer);

            return AddMicrowaveOven(microwaveOven, key);
        }

        /// <summary>
        /// Creates new MicrowaveOven.
        /// </summary>
        /// <param name="maxValueOfTimer">Max value, that timer can have, expressed in milliseconds</param>
        /// <param name="interval">Ttime value of MicrowaveTimer for one tick event, expressed in milliseconds</param>
        /// <param name="key">Unique name of MicrowaveOven</param>
        /// <returns>Returns unique name of MicrowaveOven</returns>
        public String CreateNewMicrowaveOven(int maxValueOfTimer, int interval, String key = null)
        {
            MicrowaveOven microwaveOven = new MicrowaveOven(maxValueOfTimer, interval);

            return AddMicrowaveOven(microwaveOven, key);
        }

        /// <summary>
        /// Remove MicrowaveOven by unique key.
        /// </summary>
        /// <param name="key">Unique name of MicrowaveOven</param>
        public void RemoveMicrowaveOven(String key)
        {
            if (!microwaveOvensDictionary.Remove(key))
            {
                throw new NotFoundKeyException("The key value does not exists.", $"Can't remove MicrowaveOven by key: {key}");
            }
        }

        /// <summary>
        /// Opens MicrowaveOven door based on unique MicrowaveOven key.
        /// </summary>
        /// <param name="key">Unigue name of MicrowaveOven</param>
        public void OpenDoor(String key)
        {
            MicrowaveOven microwave = GetMicrowaveOven(key);
            microwave.OpenOrCloseDoorAction(true);
        }

        /// <summary>
        /// Closes MicrowaveOven door based on unique MicrowaveOven key.
        /// </summary>
        /// <param name="key">Unigue name of MicrowaveOven</param>
        public void CloseDoor(String key)
        {
            MicrowaveOven microwave = GetMicrowaveOven(key);
            microwave.OpenOrCloseDoorAction(false);
        }

        /// <summary>
        /// Press StartButton of MicrowaveOven based on unique MicrowaveOven key.
        /// </summary>
        /// <param name="key">Unigue name of MicrowaveOven</param>
        public void PressStartButton(String key)
        {
            MicrowaveOven microwave = GetMicrowaveOven(key);
            microwave.PressStartButton();
        }

        /// <summary>
        /// Gets MicrowaveOven heater state.
        /// </summary>
        /// <param name="key">Unigue name of MicrowaveOven</param>
        /// <returns>Heater State</returns>
        public bool IsMicrowaveOvenRunning(String key)
        {
            return GetMicrowaveOven(key).HeaterRunning;
        }

        /// <summary>
        /// Gets Current time value of MicrowaveOven timer based on unique key.
        /// </summary>
        /// <param name="key">Unigue name of MicrowaveOven</param>
        /// <returns>Current time value, expressed in milliseconds.</returns>
        public int GetTimerCurrentValue(String key)
        {
            return GetMicrowaveOven(key).CurrentTimeValue;
        }

        /// <summary>
        /// Gets LightState of MicrowaveOven based on unique key.
        /// </summary>
        /// <param name="key">Unigue name of MicrowaveOven</param>
        /// <returns>LightState of MicrowaveOven.</returns>
        public MicrowaveOven.LightState GetLightState(String key)
        {
            return GetMicrowaveOven(key).Light;
        }

        /// <summary>
        /// Gets DoorState of MicrowaveOven based on unique key.
        /// </summary>
        /// <param name="key">Unigue name of MicrowaveOven</param>
        /// <returns>DoorState of MicrowaveOven.</returns>
        public bool GetDoorState(String key)
        {
            return GetMicrowaveOven(key).DoorOpen;
        }

        #endregion
    }
}
