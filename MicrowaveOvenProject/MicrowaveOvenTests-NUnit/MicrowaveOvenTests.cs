﻿using NUnit.Framework;
using MicrowaveOvenProject.Model;

namespace MicrowaveOvenTests_NUnit
{
    [TestFixture]
    public class MicrowaveOvenTests
    {
        private MicrowaveOven microwaveOven;

        [SetUp]
        public void MicrowaveOvenTestsSetUp()
        {
            microwaveOven = new MicrowaveOven(MicrowaveOven.MinuteInMilliseconds * 2);
        }

        [TestCase(true)]//for open
        [TestCase(false)]//for close
        public void DoorAreOpenedOrClosed(bool value)
        {
            microwaveOven.OpenOrCloseDoorAction(value);

            Assert.AreEqual(value, microwaveOven.DoorOpen);
        }

        [TestCase(true, MicrowaveOven.LightState.On)]
        [TestCase(false, MicrowaveOven.LightState.Off)]
        public void LightIsTurnOnOrOffTest(bool doorOpen, MicrowaveOven.LightState lightState)
        {
            microwaveOven.OpenOrCloseDoorAction(doorOpen);

            Assert.AreEqual(microwaveOven.Light, lightState);
        }

        [Test]
        public void OpenDoorWhenHeaterRunning()
        {
            microwaveOven.PressStartButton();
            microwaveOven.OpenOrCloseDoorAction(true);

            Assert.AreEqual(false, microwaveOven.HeaterRunning);
        }

        [Test]
        public void PressStartButtonWhenDoorOpenedTest()
        {
            microwaveOven.OpenOrCloseDoorAction(true);

            microwaveOven.PressStartButton();

            Assert.AreEqual(false, microwaveOven.HeaterRunning);
        }

        [Test]
        public void PressStartButtonWhenDoorClosedAndHeaterStoppedTest()
        {
            microwaveOven.OpenOrCloseDoorAction(false);
            microwaveOven.TurnOffHeater();

            microwaveOven.PressStartButton();
            int time = microwaveOven.CurrentTimeValue;

            Assert.AreEqual(MicrowaveOven.MinuteInMilliseconds, time);
        }

        [Test]
        public void PressStartButtonWhenDoorClosedAndHeaterAlreadyHeatingTest()
        {
            microwaveOven.OpenOrCloseDoorAction(false);

            microwaveOven.PressStartButton();
            microwaveOven.PressStartButton();

            int time = microwaveOven.CurrentTimeValue;

            Assert.AreEqual(MicrowaveOven.MinuteInMilliseconds * 2, time);
        }
    }
}
