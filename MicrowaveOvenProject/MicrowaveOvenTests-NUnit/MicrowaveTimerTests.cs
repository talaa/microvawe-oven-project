﻿using System.Timers;
using NUnit.Framework;
using NSubstitute;
using MicrowaveOvenProject.Model;
using MicrowaveOvenProject.CustomExceptions;


namespace MicrowaveOvenTests_NUnit
{
    [TestFixture]
    public class MicrowaveTimerTests
    {
        [TestCase(1000, 1000, 0)]
        [TestCase(3000, 1000, 2000)]
        [TestCase(6000, 2000, 4000)]
        [TestCase(7000, 7000, 0)]
        public void MicrowaveTimerTickTest(int startValue, int interval, int endValue)
        {
            IMicrowaveTimer timer = Substitute.For<IMicrowaveTimer>();

            int value = timer.CurrentValue = startValue;
            timer.Interval = interval;

            timer.Elapsed += (sender, e) => value -= interval;
            timer.Elapsed += Raise.Event<ElapsedEventHandler>(this, null);

            Assert.AreEqual(endValue, value);
        }

        [Test]
        public void MicrowaveTimerIsTimerStoppedTest()
        {
            bool isStopped = true;

            IMicrowaveTimer timer = Substitute.For<IMicrowaveTimer>();

            timer.TimerStopped += (sender, e) => isStopped = timer.Enabled;
            timer.TimerStopped += Raise.Event();

            Assert.AreEqual(false, isStopped);
        }

        [Test]
        public void MicrowaveTimerIsTimeElapsedTest()
        {
            int value = -1;

            IMicrowaveTimer timer = Substitute.For<IMicrowaveTimer>();

            timer.TimerStopped += (sender, e) => value = timer.CurrentValue;
            timer.TimerStopped += Raise.Event();

            Assert.AreEqual(0, value);
        }

        [TestCase(2000, 3000)]
        public void SetTimeBiggerThanMaxTime(int maxTime, int timeToSet)
        {
            IMicrowaveTimer timer = Substitute.For<IMicrowaveTimer>();
            timer.MaxTimeValue = maxTime;

            timer.When(t => t.AddTime(timeToSet)).Do(t => throw new InvalidValueException(""));

            Assert.Throws<InvalidValueException>(() => timer.AddTime(timeToSet));
        }
    }
}
