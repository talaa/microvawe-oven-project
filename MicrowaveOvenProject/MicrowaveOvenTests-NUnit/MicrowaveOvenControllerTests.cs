﻿using System;
using NUnit.Framework;
using MicrowaveOvenProject.Controller;
using MicrowaveOvenProject.CustomExceptions;
using MicrowaveOvenProject.Model;

namespace MicrowaveOvenTests_NUnit
{
    [TestFixture]
    public class MicrowaveOvenControllerTests
    {
        private const int MinuteInMilliseconds = 60_000;

        private MicrowavesOvenController controller;

        [SetUp]
        public void MicrowaveOvenTestSetUp()
        {
            controller = new MicrowavesOvenController();
        }

        [TestCase(null)]
        [TestCase("newMicroOven")]
        public void MicrowaveOvenCreatedTest(String key)
        {
            int startNumberOfElements = controller.Count;

            controller.CreateNewMicrowaveOven(MinuteInMilliseconds * 2, key);

            int endNumberOfElements = controller.Count;

            Assert.AreEqual(1, endNumberOfElements - startNumberOfElements);
        }

        [TestCase("newMicroOven")]
        public void MicrowaveOvenDuplicatedKeyTest(String key)
        {
            int maxValueOfTimer = MinuteInMilliseconds * 2;
            controller.CreateNewMicrowaveOven(maxValueOfTimer, key);

            Assert.Throws<DuplicatedKeyException>(() => controller.CreateNewMicrowaveOven(maxValueOfTimer, key));
        }

        [TestCase(null)]
        [TestCase("newMicroOven")]
        public void MicrowaveOvenRemovedTest(String key)
        {
            String createdKey = controller.CreateNewMicrowaveOven(MinuteInMilliseconds * 2, key);

            int startNumberOfElements = controller.Count;

            controller.RemoveMicrowaveOven(createdKey);

            int endNumberOfElements = controller.Count;

            Assert.AreEqual(startNumberOfElements - 1, endNumberOfElements);
        }

        [Test]
        public void MicrowaveOvenRemovedIfKeyNotExistsTest()
        {
            controller.CreateNewMicrowaveOven(MinuteInMilliseconds * 2, "microwave1");

            String keyToRemove = "microwave2";

            Assert.Throws<NotFoundKeyException>(() => controller.RemoveMicrowaveOven(keyToRemove));
        }

        [Test]
        public void MicrowaveOvenOpenDoorTest()
        {
            String key = controller.CreateNewMicrowaveOven(MinuteInMilliseconds * 2);

            controller.OpenDoor(key);

            bool doorState = controller.GetDoorState(key);

            Assert.AreEqual(true, doorState);
        }

        [Test]
        public void MicrowaveOvenCloseDoorTest()
        {
            String key = controller.CreateNewMicrowaveOven(MinuteInMilliseconds * 2);

            controller.CloseDoor(key);

            bool doorState = controller.GetDoorState(key);

            Assert.AreEqual(false, doorState);
        }

        [Test]
        public void MicrowaveOvenLightStateWhenDoorOpenedTest()
        {
            String key = controller.CreateNewMicrowaveOven(MinuteInMilliseconds * 2);

            controller.OpenDoor(key);

            MicrowaveOven.LightState lightState = controller.GetLightState(key);

            Assert.AreEqual(MicrowaveOven.LightState.On, lightState);
        }

        [Test]
        public void MicrowaveOvenLightStateWhenDoorClosedTest()
        {
            String key = controller.CreateNewMicrowaveOven(MinuteInMilliseconds * 2);

            controller.CloseDoor(key);

            MicrowaveOven.LightState lightState = controller.GetLightState(key);

            Assert.AreEqual(MicrowaveOven.LightState.Off, lightState);
        }

        [Test]
        public void MicrowaveOvenRunningWhenDoorAreOpeningTest()
        {
            String key = controller.CreateNewMicrowaveOven(MinuteInMilliseconds * 2);
            controller.CloseDoor(key);

            controller.PressStartButton(key);
            controller.OpenDoor(key);

            bool isRunning = controller.IsMicrowaveOvenRunning(key);

            Assert.AreEqual(false, isRunning);
        }

        [Test]
        public void MicrowaveOvenIsRunningWhenDoorOpened()
        {
            String key = controller.CreateNewMicrowaveOven(MinuteInMilliseconds * 2);
            controller.OpenDoor(key);

            controller.PressStartButton(key);

            bool isRunning = controller.IsMicrowaveOvenRunning(key);

            Assert.AreEqual(false, isRunning);
        }

        [Test]
        public void MicrowaveOvenIsRunningWhenDoorClosed()
        {
            String key = controller.CreateNewMicrowaveOven(MinuteInMilliseconds * 2);
            controller.CloseDoor(key);

            controller.PressStartButton(key);

            bool isRunning = controller.IsMicrowaveOvenRunning(key);

            Assert.AreEqual(true, isRunning);
        }

        [Test]
        public void MicrowaveOvenTimerCurrentValueWhenStartPressedAndIsNotRunningBeforeTest()
        {
            String key = controller.CreateNewMicrowaveOven(MinuteInMilliseconds * 2);

            controller.PressStartButton(key);
            int currentValue = controller.GetTimerCurrentValue(key);

            Assert.AreEqual(MinuteInMilliseconds, currentValue);
        }

        [Test]
        public void MicrowaveOvenTimerCurrentValueWhenStartPressedAndIsAlreadyRunningTest()
        {
            String key = controller.CreateNewMicrowaveOven(MinuteInMilliseconds * 2);
            controller.PressStartButton(key);

            controller.PressStartButton(key);
            int currentValue = controller.GetTimerCurrentValue(key);

            Assert.AreEqual(MinuteInMilliseconds * 2, currentValue);
        }
    }
}
